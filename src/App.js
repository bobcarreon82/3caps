import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import NavBar from './components/NavBar';
import Register from './pages/Register';
import Login from './pages/Login'; 
import Home from './pages/Home';
import Products from './pages/Products';
import SpecificProduct from './pages/SpecificProduct'
import { UserProvider } from './UserContext';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import OrderProp from './pages/OrderProp';
import Cart from './pages/Cart2';
// import OrderHistory from './pages/OrderHistory'
import Orders from './pages/Orders'



export default function App()  {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser =() =>{
    localStorage.clear()
    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data =>{
      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id:null,
          isAdmin: null
        })
      }
    })
  }, [])


  return (
 <UserProvider value={{user, setUser, unsetUser}}>  
    <Router>
      <NavBar/>
      <Switch>
        <Route exact path ="/" component={Home}/>
        <Route exact path ="/register" component={Register}/>
        <Route exact path ="/login" component={Login}/>
        <Route exact path ="/products" component={Products}/>
        <Route exact path ="/products/:productID" component={SpecificProduct}/>
        <Route exact path ="/orders" component={OrderProp}/>
        <Route exact path ="/cart" component={Cart}/>
        <Route exact path ="/orders" component={Orders}/>
        {/* <Route exact path ="/products/:productId" component={SpecificProduct}/> */}
      </Switch>
    </Router>
  </UserProvider>
  )
}
