import { Row, Col, Jumbotron,Button, Card, Carousel } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import './Banner.css'


export default function Banner({bannerProps}){
	const {title, content, destination, label} = bannerProps;
	return(

<div className="car">
<Carousel>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src='./images/vax1.5.jpg'
      alt="First slide"
    />
    <Carousel.Caption>
      <h3>Top Vaccine Distributor</h3>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src='./images/vax2.5.jpg'
      alt="Second slide"
    />

    <Carousel.Caption>
      <h3>Only The Best Quality Vaccines</h3>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src='./images/vax3.5.jpg'
      alt="Third slide"
    />

    <Carousel.Caption>
      <h3>24/7 Ordering System</h3>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>

</div>

		)
}

