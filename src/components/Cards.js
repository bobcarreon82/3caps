import { Row, Col, Card, Button } from 'react-bootstrap';
import './Cards.css'


const Cards = props =>{
	return (
		<div className="card text-center shadow">
			<div className="overflow">
				<img src={props.imgsrc} alt='Image1' className="card-img-top"/>
			</div>

			<div className="card-body text-dark">
				<h4 className="card-title">{props.title}</h4>
				<p className="card-text text-secondary">
				{props.paragraph}
				</p>
				<a href="#" className="btn btn-outline-success">Go Anywhere</a>
			</div>
		</div>
	); 
}

export default Cards; 