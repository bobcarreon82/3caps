import React from 'react';
import './Footer.css'

export default function Footer(){
    return(
        <div className='main-footer'>
                <div className='container'>
                    <div className='row'>
                        {/* Column 1 */}
                        <div className='col'>
                            <h7>Vaccine Order</h7>
                            <ul className='list-unstyled'>
                                <li>0917-888-888</li>
                                <li>Manila, Philippines</li>
                                <li>Alabang, Muntinlupa City</li>
                            </ul>
                        </div>

                        {/* Column 2 */}
                        <div className='col'>
                            <h7>Hours of Operations</h7>
                            <ul className='list-unstyled'>
                                <li>M-F: 6am-3pm</li>
                                <li>S-Su: 6am-12noon</li>
                                
                            </ul>
                        </div>

                        {/* Column 3 */}
                        <div className='col'>
                            <h7>Socials</h7>
                            <ul className='list-unstyled'>
                                <li>Facebook</li>
                                <li>Instagram</li>
                                <li>Linked In</li>
                            </ul>
                        </div>

                    </div>
                    <hr/>
                    <div className='endfooter'>
                        <div className='row'>
                            <p className='col-sm'>
                                &copy;{new Date().getFullYear()} VACCINE ORDER INC | All rights reserves | Terms of Service | Privacy
                            </p>

                        </div>
                    </div>

                </div>
        </div>
    )
}
