import { Row, Col, Card, Button } from 'react-bootstrap';
import React, {Component} from 'react'
import Cards from './Cards';



export default function Highlights(){
	return (
		<div className="container-fluid d-flex justify-content-center">
				<div className="row">
					<div className="col-md-4">
					   <Cards imgsrc='./images/1.jpg' title='#1 Choice of Doctors' paragraph='We continue to be the top of mind when it comes to vaccine needs of physicians'/>
					</div>	
					<div className="col-md-4">
					   <Cards imgsrc='./images/2.jpg' title='Promote Health' paragraph='We promote healthy and virus free living  through the help of vaccines' />
					</div>	
					<div className="col-md-4">
					   <Cards imgsrc='./images/3.jpg' title='Cold Chain' paragraph='You are guaranteed the best quality vaccines through our cold chain management system'/>
					</div>	
				</div>				
		</div>
	)
}


// export default function Highlights(){
// 	return(

// <div className="card">	

//       <Row> 
// 			<Col xs={12} md={4}>
// 				<Card className ="card-highlight">
// 					<Card.Body>
// 						<Card.Title>Influenza</Card.Title>
// 						<Card.Text>
// 						Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam consequuntur ea, animi, rem voluptate voluptas provident quasi alias laborum optio ad soluta molestiae. Non mollitia praesentium distinctio? Ab, quaerat ea.
// 						</Card.Text>
// 					</Card.Body>
// 				 </Card>
// 			 </Col>
// 			<Col xs={12} md={4}>
// 				<Card className ="card-highlight">
// 					<Card.Body>
// 						<Card.Title>Cervical Cancer</Card.Title>
// 						<Card.Text>
// 						Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam consequuntur ea, animi, rem voluptate voluptas provident quasi alias laborum optio ad soluta molestiae. Non mollitia praesentium distinctio? Ab, quaerat ea.
// 						</Card.Text>
// 					</Card.Body>
// 				 </Card>
// 			 </Col>
// 			<Col xs={12} md={4}>
// 				<Card className ="card-highlight">
// 					<Card.Body>
// 						<Card.Title>Rotavirus</Card.Title>
// 						<Card.Text>
// 						Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam consequuntur ea, animi, rem voluptate voluptas provident quasi alias laborum optio ad soluta molestiae. Non mollitia praesentium distinctio? Ab, quaerat ea.
// 						</Card.Text>
// 					</Card.Body>
// 				 </Card>
// 			 </Col>
// 		</Row>


// </div>




// 	)
// }

