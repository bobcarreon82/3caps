import { Navbar, Nav, Form, FormControl, Button} from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import {useContext, useState} from 'react';
import UserContext from '../UserContext';
//added
import {itemTotal} from './cartHelpers'


export default function NavBar() {

    const { user, unsetUser } = useContext(UserContext)
    const history = useHistory()
  
    const logout = () => {
      unsetUser()
      //useHistory.push redirects the user to the given location
      history.push('/login')
    }
  
    let rightNav = (!user.id) ? (
  
        <>
            <Link className="nav-link" to="/register">Register</Link>
            <Link className="nav-link" to="/login">Log In</Link>
        </>
  
      ) : (
            <Nav.Link onClick={logout}>Log Out</Nav.Link>
      )
  
return(

<Navbar bg="light" expand="lg">
    <Link className="navbar-brand" to="/">Vaccine Order <i class="fas fa-medkit"></i> </Link>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="ml-auto">
        <Link className="nav-link" to="/">Home</Link>
        <Link className="nav-link" to="/products">Products</Link>
        <Link className="nav-link" to="/orders">Orders</Link>
        <Link className="nav-link" to="/cart">{itemTotal()}Cart</Link>
        {rightNav}
      </Nav>
    </Navbar.Collapse>
  </Navbar>




)

}