import { useState, useEffect } from "react";
import {Card, Button, Container} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Order({orderProp}) {
    const {_id, totalAmount, userId, itemToOrder} = orderProp

    return(
        <Card>
            <Card.Body>
                <Card.Title>{}</Card.Title>
                <Card.Text>
                    {}
                </Card.Text>

                <p>Ordered Item: {itemToOrder.mode}</p>
                <p>Amount Paid: {totalAmount}</p>
            </Card.Body>

        </Card>
    )
}

// const {_id, totalAmount, purchasedOn, userId, itemToOrder} = orderProp