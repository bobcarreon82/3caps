import {useEffect, useState} from 'react';

import {Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';



export default function Product({productProp}){
	const {_id, name, description, manufacturer, price, expirationDate } = productProp 

	// const [count, setCount] = useState(0); 
	// const [seatsCount, setSeatsCount] =useState(30);

	// const [isDisabled, setIsDisabled] = useState(false);

	// const enroll =() =>{
	// 	setCount(count +1);
	// 	setSeatsCount(seatsCount -1)
	// };

	// useEffect(()=>{
	// 	if(seatsCount === 0){
	// 		setIsDisabled(true);
	// 	}
	// }, [seatsCount])

	return(

		             <Card >
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<h6>Description</h6>
							<p>{description}</p>
                            <h6>manufacturer</h6>
							<p>{manufacturer}</p>
							<h6>Price</h6>
							<p>{price}</p>
                            <h6>Expiration Date</h6>
							<p>{expirationDate}</p>
                            
                            {

							}
							<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
						</Card.Body>	
					</Card>	


	)
}



// <Card >
// <Card.Body>
// 	<Card.Title>{name}</Card.Title>
// 	<h6>Description</h6>
// 	<p>{description}</p>
// 	<h6>manufacturer</h6>
// 	<p>{manufacturer}</p>
// 	<h6>Price</h6>
// 	<p>{price}</p>
// 	<h6>Expiration Date</h6>
// 	<p>{expirationDate}</p>
	
// 	{

// 	}
// 	<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
// </Card.Body>	
// </Card>	