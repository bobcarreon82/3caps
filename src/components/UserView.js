
import Product from './Product';
import { Container, Card, Button } from 'react-bootstrap';
import React, {useState, useEffect} from "react";

export default function UserView({productsData}){
	const [products, setProducts] = useState([])
	
	useEffect(() => {
		
		const productsArr = productsData.map(product => {
			if(product.isActive === true){
				return (
					<Product productProp={product} key={product._id}/>	
				)
			}else{
				return null;

			}

		
		});

		setProducts(productsArr)

	}, [productsData])

	return(

		<>
        <Container>
        {products} 
        </Container>

		</>

	);
}