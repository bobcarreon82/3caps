// import { truncate, truncateSync } from 'fs';
// import {useState, useContext} from 'react';
import React, {useState, useContext} from "react";
import UserContext from '../UserContext';
import {Form, Button, Container} from 'react-bootstrap';
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2';



export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const {user, setUser} = useContext(UserContext)
	const [loginButton, setLoginButton] = useState(false);


	const logInUser = (e) =>{
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})


	}

	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
			  Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	if(user.id != null ){
		return<Redirect to="/"/>
	}


	return(

	<Container>	
		<Form className="mt-3" onSubmit = {(e) => logInUser(e)}>	
			<h4>Log In</h4>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder= "Enter Email" value={email} onChange={e =>setEmail(e.target.value)} required/>
			</Form.Group>
			
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder= "Enter Password" value={password} onChange={e =>setPassword(e.target.value)} required/>
			</Form.Group>
			<Button variant="primary" type="submit"> Log In </Button>
		</Form>
	</Container>

	)
}