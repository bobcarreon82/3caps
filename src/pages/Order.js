import React from 'react';																	
import {useEffect, useState} from 'react';																	
import {Container} from 'react-bootstrap';																	
																	
export default function Order({ordersData})																	
{																	
	const [orders, setOrders] = useState([]);																
	const [productList, setProductList] = useState([]);																
																	
	function formatDate(date) {																
	var d = new Date(date),																
	month = '' + (d.getMonth() + 1),																
	day = '' + d.getDate(),																
	year = d.getFullYear();																
																	
	if (month.length < 2)																
	month = '0' + month;																
	if (day.length < 2)																
	day = '0' + day;																
																	
	return [year, month, day].join('-');																
	}																
																	
																	
																	
	useEffect(() => {																
																	
		console.log(ordersData);															
																	
		let orderCount = 1;															
																	
		const oderArr = ordersData.map(order => {															
																	
			const productOrder = order.orderedItem.map(orderItem => {														
																	
				return(													
																	
					<li>{orderItem.name} - Quantity: {orderItem.quantity}</li>												
				)													
																	
			});														
																	
			// productOrder.join(' ');														
																	
			let date = formatDate(order.dateOrder)														
																	
			return(														
																	
				<>													
					<div class="bg-secondary text-white card-header">Order #{orderCount} - Purchased on: {date}												
					</div>												
					<div class="card-body">												
						<h6>Items:</h6>											
																	
						<ul>											
							{productOrder}										
						</ul>											
																	
						<h6>											
							Total Quantity:										
							<span class="text-warning"> {order.totalQuantity}</span></h6>											
																	
						<h6>											
							Total:										
							<span class="text-warning"> ₱{order.totalAmount}</span></h6>											
					</div>												
				</>													
																	
			)														
																	
			orderCount++;														
		})															
																	
		setOrders(oderArr);															
																	
	},[ordersData])																
																	
																	
																	
	return(																
		<Container>															
																	
			<h2 className="text-center my-4">Order History</h2>														
																	
			<div class="accordion">														
				<div class="card">													
					{orders}												
				</div>													
			</div>														
		</Container>															
	)																
}																	
																	