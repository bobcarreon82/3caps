import OrderHistory from './HistoryOrder'
import Order from './Order';										
import { useState, useEffect} from 'react';		
import UserContext from '../UserContext';
import { useContext} from 'react';								
										
export default function OrderProp()										
{										
	const [orders, setOrders] = useState([]);	
    const { user } = useContext(UserContext);								
										
	const fetchDataOrder = () => {									
				
        console.log(user)

		fetch(`${process.env.REACT_APP_API_URL}/orders/myOrder`,{
			headers: {
			Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})								
		.then(res => res.json())								
		.then(data => {
            
        console.log(data)    
										
			let orderArr = [];							
										
						
			data.forEach(order => {							
										
				let orderPs = [];						
															
										
				let allOrd = {						
										
					dateOrder : order.createdOn,					
					orderedItem : orderPs,					
					totalQuantity : order.quantity,					
					// totalAmount : order.totalAmount					
				}						
										
				orderArr.push(allOrd);						
										
			})							
										
			console.log(orderArr);							
			setOrders(orderArr);							
										
		})
        								
	}									
										
	useEffect(() => {									
										
		fetchDataOrder();								
		console.log(orders);								
										
	},[])									
										
										
										
	return(									
										
		<>	
            						
			<Order ordersData={orders}/>							
		</>								
										
	)									
}										
