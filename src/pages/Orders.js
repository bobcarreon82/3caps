import React, {useState, useEffect, useContext} from "react";
import {Container} from 'react-bootstrap';
import UserView from "../components/UserView";
import AdminView from "../components/AdminView";
import UserContext from '../UserContext';
import History from "./History";


export default function Order(){

	const { user } = useContext(UserContext);

	const [products, setProducts] = useState([])
	
	const fetchData =() => {
		fetch (`${process.env.REACT_APP_API_URL}/orders/myOrder`
        ,{headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
        })
		.then(res => res.json())
		.then(data => {
            console.log(data)
            
			setProducts(data)
					  
		});

	}

	useEffect(() => {
		fetchData()
	}, [])




	return(
		<>
		<Container>
       
       <History orderedItems = {products} fetchData={fetchData}/>
       
		</Container>
		</>
	);
}