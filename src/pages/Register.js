import UserContext from '../UserContext';
import React, {useState, useEffect, useContext} from 'react';
// import {useContext} from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import {Redirect, useHistory} from 'react-router-dom'
import Swal from 'sweetalert2';


export default function Register() {

const [firstName, setFirstName] = useState('')
const [lastName, setLastName] = useState('')
const [prcNumber, setPrcNumber] = useState('')
// const [tin, setTin] = useState('')
const [deliveryAddress, setDeliveryAddress] = useState('')
const [mobileNo, setMobileNo] = useState('')
const [email, setEmail] = useState('')
const [password, setPassword] = useState('')
const [verifyPassword, setVerifyPassword] = useState('');
const [registerButton, setRegisterButton] = useState(false);
const {user, setUser} = useContext(UserContext)
// const {user, setUser} = useContext(UserContext)
const history = useHistory(); 

function registerUser(e){
e.preventDefault();
  fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      prcNumber: prcNumber,
      deliveryAddress: deliveryAddress,
      mobileNo: mobileNo,
      email: email,
      password: password, 

    })
  })
  .then(res => res.json())
  .then(data => {
    if(data === true){
      Swal.fire({
        title: 'Registration successful',
        icon: 'success',
        text: 'Welcome to Zuittr'
    })
    } else {
      Swal.fire({
        title: 'Something went wrong',
        icon: 'error',
        text: 'Try again'
    })
    }
  })
}

//if the functional component makes calculations that don't target the output value, then calculations are named side effects. Example of side-effect are fetch requests, manipulating state changes/updates

useEffect(()=>{
//let's modify our current logic to validate the data inserted by the user

if((email !=='' && password !== '' && verifyPassword !=='') && (password === verifyPassword)){

    setRegisterButton(true)

    
}else{
    setRegisterButton(false)

}
},[email, password, verifyPassword])

  //Make our Register page redirect any logged in users that try to access the page - TO BE ADDED

  if(user.id !== null ){
		return<Redirect to="/"/>
	}

    return(
    <Container>
        <Form className="mt-3" onSubmit={ (e) => registerUser(e)}>
        <Form.Group>
            <Form.Label>First Name</Form.Label>
            <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
        </Form.Group>    

        <Form.Group>
            <Form.Label>Last Name</Form.Label>
            <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
        </Form.Group>

        <Form.Group>
            <Form.Label>PRC Number</Form.Label>
            <Form.Control type="text" placeholder="Enter last name" value={prcNumber} onChange={e => setPrcNumber(e.target.value)} required/>
        </Form.Group>

        <Form.Group>
            <Form.Label>Delivery Address</Form.Label>
            <Form.Control type="text" placeholder="Enter last name" value={deliveryAddress} onChange={e => setDeliveryAddress(e.target.value)} required/>
        </Form.Group>


        <Form.Group>
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control type="text" placeholder="Enter mobile number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
        </Form.Group>    

    
        <Form.Group>
            <Form.Label>Email Address</Form.Label>
            <Form.Control type="email" placeholder ="Enter Email" value={email} onChange={e =>setEmail(e.target.value)} required/>
            <Form.Text className="text-muted">
                We'll never share your email with anyone else
            </Form.Text>
        </Form.Group>
      
        <Form.Group>
            <Form.Label>Password:</Form.Label>
            <Form.Control type="password" placeholder ="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
        </Form.Group>

        <Form.Group>
            <Form.Label>Verify Password:</Form.Label>
            <Form.Control type="password" placeholder ="Verify Password" value={verifyPassword} onChange={e=> setVerifyPassword(e.target.value)} required/>
        </Form.Group>

        {registerButton ? 
         <Button variant="primary" type="submit">Submit</Button>           
        :             
        <Button variant="primary" type="submit">Submit</Button>           
        }



    </Form>

    </Container>
    )

}
