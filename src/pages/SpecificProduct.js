import { regExpLiteral } from '@babel/types';
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import { Link, useParams, useHistory, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import {addItem} from '../components/cartHelpers' 
//added add item

import UserContext from '../UserContext';

export default function SpecificProduct(){

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
    const [manufacturer, setManufacturer] = useState("")
    const [expirationDate, setExpirationDate] = useState("")
	const [id, setId] = useState("")
	const [qty, setQty] = useState(1)


	const { user } = useContext(UserContext);

	const { productID} = useParams();
	
	const history = useHistory();

	useEffect(()=> {


		fetch(`${ process.env.REACT_APP_API_URL }/products/${productID}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setQty(qty)
			setId(productID)
            setManufacturer(data.manufacturer)
            setExpirationDate(data.expirationDate)
		})

	}, [])

	const addToCart =() => {
			//variable to determine if the item we are adding is already in our cart or not
	let alreadyInCart = false
	//variable for the item's index in the cart array, if it already exists there
	let productIndex
	//temporary cart array
	let cart = []

	if(localStorage.getItem('cart')){
		cart = JSON.parse(localStorage.getItem('cart'))
	}

	//loop through our cart to check if the item we are adding is already in our cart or not
	for(let i = 0; i < cart.length; i++){
		if(cart[i].productId === id){
			//if it is, make alreadyInCart true
			alreadyInCart = true
			productIndex = i
		}
	}

	//if a product is already in our cart, just increment its quantity and adjust its subtotal
	if(alreadyInCart){
		cart[productIndex].quantity += qty
		cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
	}else{
		//else add a new entry in our cart, with values from states that need to be set wherever this function goes
		cart.push({
			'productId' : id,
			'name': name,
			'price': price,
			'quantity': qty,
			'subtotal': price * qty
		})		
	}

	//set our localStorage cart as well
	localStorage.setItem('cart', JSON.stringify(cart))

	}

	const enroll =() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/purchase`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem ('token')}`
			},
			body: JSON.stringify({
				productId: productID,
				quantity: qty
			})

		})
		.then(res => res.json())
		.then(data => {
           console.log(data);
			if(data === true){
				Swal.fire({
					title: "Successful",
					icon: 'success',
					text: "You have successfully whatever"
				})

				addToCart();
				history.push("/cart")
				
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})

	}

	// const addToCart = () => {
	// 	addItem(product, () =>{
	// 		setRedirect(true)
	// 	})
	// }

	// const shouldRedirect = redirect => {
	// 	if(redirect) {
	// 		return <Redirect to='/cart'/> 
	// 	}
	// }

	return(
		<Container>
			<Card className="mt-5">
				<Card.Header className="bg-dark text-white text-center pb-0">
					<h4>{name}</h4>
				</Card.Header>
				<Card.Body>
				
					{/* {shouldRedirect(redirect)} */}
				
					<Card.Text>{description}</Card.Text>
                    <Card.Text><h5>Manufacturer:</h5> <h7>{manufacturer}</h7></Card.Text>
                    <Card.Text><h5>Price:</h5> <h7>{price}</h7></Card.Text>
                    <Card.Text><h5>Expiration Date:</h5> <h7>{expirationDate}</h7></Card.Text>
                    <Button className="btn btn-dark" onClick={()=> setQty(qty + 1)}>+</Button><input type="number" onChange={(e)=> setQty(e.target.value
						)} value={qty}></input> <Button className="btn btn-dark" onClick={()=> setQty(qty - 1)}>-</Button>


				</Card.Body>
				<Card.Footer>
					{user.id !== null
						? <Button variant="primary" block onClick={() =>enroll()}>Order Now</Button>
						: <Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
					}
				</Card.Footer>				
			</Card>
		</Container>
	)
}


// <Card.Footer>
// {user.id !== null
// 	? <Button variant="primary" block onClick={() =>enroll(productID)}>Order Now</Button>
// 	: <Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
// }
// </Card.Footer>			